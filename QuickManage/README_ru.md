# QuickManage

Плагин для облегчения рутинной работы администраторам сервера.

## Команды
`/qm` - Вывод список команд;   
`/do <player> <text>` - Игрок `<player>` вводит `<text>` в чат. *(работает и с командами)*;   
`/glow` - На игрока, который ввел эту команду, накладывается эффект свечения;   
`/rename <text>` - Переименновывает предмет в руке на `<text>` *(поддерживает цветовые коды)*;   
`/qmreload` - Перезагрузка плагина;   
`/phys` - Отключает/включает физику блоков;   
`/bcast <text>` - Отправляет сообщение `<text>` всем игрокам без префиксов/суффиксов и пр.;   
`/bcastp <player> <text>` - Отправляет сообщение `<text>` игроку `<player>` без префиксов/суффиксов и пр.;   
`/sign <line> <text>` - Принудительно устанавливает текст `<text>` на строке `<line>` в табличке на которую вы смотрите;   
`/setname <text>` - Переименовывает моба *(ПКМ)*;   
`/gench` - Чарует предмет и ставит HIDE_ATRIBUTE *предмет переливается как зачарованный без чаров*;   
`/firework` - Запускает рандомный фейерверг.   

## Права
`quickmanage.bcast`;   
`quickmanage.bcastp`;   
`quickmanage.do`;   
`quickmanage.glow`;   
`quickmanage.gench`;   
`quickmanage.fall`;   
`quickmanage.firework`;   
`quickmanage.help`;   
`quickmanage.reload`;   
`quickmanage.rename`;   
`quickmanage.rename.color`;   
`quickmanage.setname`;   
`quickmanage.setname.color`;   
`quickmanage.sign`.