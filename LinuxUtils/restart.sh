#!/bin/bash
reload_time=10; #Время отсчета до перезагрузки
dt=$(date '+%d/%m/%Y %H:%M:%S'); #Текущая дата формата: день/месяц/год час:минута:секунда
echo "===== $dt =====" >> restart_log.txt #Запись даты в файл restart_log.txt
declare -a array 
array=([1]=auth [2]=hub-1 [3]=survival) #Название скринов для перезапуска
complite=()
bad=()

for ((i=$reload_time;i>=1;i--)); #Цикл отсчета времени до перезагрузки с оповещением
do
    if [ ${i} = "1" ]; then
        screen -S bungee -X eval 'stuff "broadcast Перезапуск сервера через '${i}' секунду"\015' #Команда `alert` в скрин `bungee`
        #screen -S server -X eval 'stuff "tm broadcast &eПерезагрузка через %nl%&c'${i}' секунду"\015' #Команда `tm` в скрин `server` (tm - TitleManager)
        echo "Перезапуск сервера через ${i} секунду"
        sleep 1s
    elif [ ${i} = "2" ] || [ ${i} = "3" ] || [ ${i} = "4" ]; then
        screen -S bungee -X eval 'stuff "broadcast Перезапуск сервера через '${i}' секунды"\015'
        #screen -S server -X eval 'stuff "tm broadcast &eПерезагрузка через %nl%&c'${i}' секунды"\015'
        echo "Перезапуск сервера через ${i} секунды"
        sleep 1s
    else 
        screen -S bungee -X eval 'stuff "broadcast Перезапуск сервера через '${i}' секунд"\015'
        #screen -S server -X eval 'stuff "tm broadcast &eПерезагрузка через %nl%&c'${i}' секунд"\015'
        echo "Перезапуск сервера через ${i} секунд"
        sleep 1s
    fi

done
screen -S bungee -X eval 'stuff "alert Перезапуск!"\015'
echo "Перезапуск!"
sleep 1s
screen -S bungee -X eval 'stuff "end"\015'

for ((i=1;i<=${#array[@]};i++)); #Перебор скринов из массива `array` на 6-ой строке, для ввода команды `stop`
do
    if screen -list | grep -q ${array[i]}; then
        screen -S ${array[i]} -X eval 'stuff "stop"\015' #Исполнение команды `stop` в скрине <...>
        complite+=(${array[i]}) #Запись в массив скрина <...>
        echo "Сервер ${array[i]} перезагружен" >> restart_log.txt #Запись в лог об успешной перезагрузке скрина <...>
        echo -e "\033[32mСервер \033[36m${array[i]} \033[32mперезагружен\033[0m"
    else
        bad+=(${array[i]}) #Запись в массив скрина <...>
        echo "(!) Сервер ${array[i]} не найден " >> restart_log.txt #Запись в лог об ошибке перезагрузке скрина <...> из-за его отсутствия
        echo -e "\033[32mСервер \033[36m${array[i]} \033[31mне найден!\033[0m"
    fi
done

echo "======== Сводка: ========"
echo -e "\033[32m Сервера успешно перезагружены: \033[36m${complite[@]}\033[0m"
echo "-------- Сводка: --------" >> restart_log.txt
echo "Сервера успешно перезагружены: ${complite[@]}" >> restart_log.txt

if  [ ${#bad[@]} != "0" ]; then
    echo -e "\033[31m Сервера не найдены: \033[36m${bad[@]}\033[0m"
    echo "Сервера не найдены: ${bad[@]}" >> restart_log.txt
fi

echo " " >> restart_log.txt